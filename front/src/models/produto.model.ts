export class Produto {

    codigo: number  = 0;
    nome: String = '';
    desc: String = '';
    preco: number  = 0;
}