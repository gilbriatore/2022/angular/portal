import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Produto } from '../models/produto.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastroService {

  URL = "http://localhost:3000/produtos";

  constructor(private http: HttpClient) { }

  listar(): Observable<Produto[]>{
    console.log(this.URL);
    return this.http.get<Produto[]>(this.URL);
  }

  buscarPorCodigo(codigo : number): Observable<Produto>{
    return this.http.get<Produto>(this.URL + "/" + codigo);
  }

  incluir(produto : Produto): Observable<any>{
    return this.http.post<any>(this.URL, produto);
  }

  atualizar(codigo: number, produtoAlterado: Produto) : Observable<any> {
    return this.http.put<any>(this.URL + "/" + codigo, produtoAlterado);
  }

  excluir(codigo : number) : Observable<any>{
    return this.http.delete<any>(this.URL + "/" + codigo);
  }
}
