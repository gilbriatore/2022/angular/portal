const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/db_mongo';
// const URL = 'mongodb+srv://admin:AdminAdmin123@mongodbsegnoite.opfdf.mongodb.net/admin?retryWrites=true&w=majority';


const db = mongoose.connect(URL, {
  useNewUrlParser: true,
  // useFindAndModify: false,
  useUnifiedTopology: true
});

const con = mongoose.connection;

con.on('open', function(){
  console.log('Conectado ao MongoDB!');
});

con.on('error', function(){
  console.log('Erro na conexão com o MongoDB!');
});

con.on('close', function(){
  console.log('Desconetado do MongoDB!');
});

module.exports = db;
